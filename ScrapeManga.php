<?php
require_once('db.php');
include 'simple_html_dom.php';
$pattern = '%<a href="http://mangafox.me/manga/(.*)/"%';

set_time_limit(0);

//Loads the page from the URL entered
$html = file_get_contents('mangafox.html');
preg_match_all($pattern, $html, $matches);
$manga_pages = $matches[1];


class Manga {
  private $page;

  public function __construct($page) {
    $this->page = new simple_html_dom();
    $this->page->load_file('http://mangafox.me/manga/' . $page);
  }

  public function title() {
    $title = $this->page->find('div[id=title] h1');
    foreach ($title as $t) {
        $title = explode(" Manga", $t->plaintext);
        return $title[0];
    }
  }

  public function desc() {
    $description = $this->page->find('p.summary');
    if(!isset($description[0])) {
      return null;
    }
    $d = $description[0];
    return $desc = $d->plaintext;
  }

  public function status() {
    $status = $this->page->find('div[class=data] span');
    foreach ($status as $s) {
        $status = explode(",", $s->plaintext);
        return $status[0];
    }
  }

  public function genre() {
    $genre = $this->page->find('//*[@id="title"]/table/tbody/tr[2]/td[4]');
    foreach ($genre as $g) {
        return $g->plaintext;
    }
  }

  public function author() {
    $author = $this->page->find('//*[@id="title"]/table/tbody/tr[2]/td[2]');
    foreach ($author as $a) {
        return $a->plaintext;
    }
  }

  public function release() {
    $release = $this->page->find('//*[@id="title"]/table/tbody/tr[2]/td[1]');
    foreach ($release as $r) {
        return $r->plaintext;
    }
  }

  public function image() {
    $image = $this->page->find('.cover img');
    foreach ($image as $i) {
        return $i->src;
    }
  }
}

//$con=mysqli_connect("localhost","hashmkb_hash","hash@123","hashmkb_mangatracker");
// Check connection
/*if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}*/

foreach($manga_pages as $href) {
    $manga = new Manga($href);

    //echo 'Title: ' . 'Sorry, you need to find a way to get this one from $manga->title() or something :p'. '<br />';
    /*echo 'Title: ' . $manga->title(). '<br />';
    echo 'Link: ' . $href . '<br />';
    echo 'Description: ' . $manga->desc() . '<br />';
    echo 'Status: ' . $manga->status() . '<br />';
    echo 'Genre: ' . $manga->genre() . '<br />';
    echo 'Author: ' . $manga->author() . '<br />';
    echo 'Release: ' . $manga->release() . '<br />';
    echo 'Image Link: ' . $manga->image() . '<br />';
    echo '<br /><br />';
    */

$image = $manga->image();
$desc = $manga->desc();

if (!empty($image)) {
      //echo '<b>Image:</b> ' . $mangamerge['Image'] . '<br />';
      $url = explode("?", $manga->image());
      $newurl = file_get_contents($url[0]);
      $img_path = './images/cover/' . preg_replace('/[^\da-z]/i', "", strtolower($manga->title())) . mt_rand().'.jpg';
      file_put_contents($img_path, $newurl);
      $image = $img_path;
    }

    if (!empty($desc)) {
      $desc = mysqli_real_escape_string($conn, $manga->desc());
      $sql = "INSERT INTO manga(title, description, status, genre, author, release_date, cover, latest_chapter, url, added_date) VALUES 
      ('" . $manga->title() . "','".  $desc . "','" . $manga->status() . "', '" . $manga->genre() . "', '" . $manga->author() . "', '" . $manga->release() . "', '" . $image . "', '0', '" . $href . "', '" . date("Y-m-d") . "')"; 
      //mysqli_query($con,$sql);
      //echo $sql;
    } else {
      $sql = "INSERT INTO manga(title, description, status, genre, author, release_date, cover, latest_chapter, url, added_date) VALUES 
      ('" . $manga->title() . "','No Description','" . $manga->status() . "', '" . $manga->genre() . "', '" . $manga->author() . "', '" . $manga->release() . "', '" . $image . "', '0', '" . $href . "', '" . date("Y-m-d") . "')"; 
      //mysqli_query($con,$sql);
      //echo $sql;
    }

    if($conn->query($sql) === TRUE){
      echo '<b>Added:</b> ' . $manga->title() . '<br />';
    } else {
      echo 'Error inserting series' . $conn->error;
    }

    unset($sql);
  
}

echo 'FUCK YES ALL DONE!!';

    
?>