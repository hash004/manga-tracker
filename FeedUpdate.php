<?php

require_once('simplepie/autoloader.php');
require_once('db.php');
require_once('scrapMangaUpdate.php');

/*$mangaFox = new SimplePie();
$mangaFox->set_feed_url('http://feeds.feedburner.com/mangafox/latest_manga_chapters?format=xml');
//$mangaFox->enable_cache(false);
$mangaFox->init();
$mangaFox->handle_content_type();

foreach ($mangaFox->get_items() as $item){
    if(strpos($item->get_title(), ' Vol ')){
        $origTitle = preg_split("/Vol |Ch /", $item->get_title());
        $title = $origTitle[0];
        $volume = trim($origTitle[1]);
        $chapter = $origTitle[2];
    } else {
        $origTitle = preg_split("/Ch /", $item->get_title());
        $title = $origTitle[0];
        $volume = "";
        $chapter = $origTitle[1];
    }
    //if(strpos($item->get_title(), ' Vol ')){
    //    $origTitle = explode(' Vol ', $item->get_title());
    //    $title = $origTitle[0];
    //    $volume = "Vol ".$origTitle[1];
    //} else {
    //    $origTitle = explode(' Ch ', $item->get_title());
    //    $title = $origTitle[0];
    //    $volume = "Ch ".$origTitle[1];
    //}
    echo "<b>Title: </b> ".$title."<br />";
    echo "<b>Volume: </b> ".$volume."<br />";
    echo "<b>Chapter: </b>".$chapter."<br />";
    echo "<b>Desc: </b> ".$item->get_description()."<br />";
    echo "<b>Date: </b> ".$item->get_date('j F Y | g:i a')."<br />";
    $link = $item->get_item_tags('http://rssnamespace.org/feedburner/ext/1.0', 'origLink');
    echo "<b>Link: </b><a href=\"".$link[0]['data']. "\">".$link[0]['data']."</a><br /><br />";

}
*/
?>

<?php

$mangaUpdate = new SimplePie();
$mangaUpdate->set_feed_url('https://www.mangaupdates.com/rss.php');
$mangaUpdate->enable_cache(false);
$mangaUpdate->init();
$mangaUpdate->handle_content_type();


foreach ($mangaUpdate->get_items() as $item) {

    //Only proceed if it isn't a Novel
    if (strpos($item->get_title(), '(Novel)') == false){
        //Use Regex to split the group name, manga name, volume, chapter e.g. [LINE Webtoon]The Gamer v.1 c.77
        $origTitle = preg_split("/(^(.*?)\])/", $item->get_title(), 0, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
        $group = explode(" &amp; ", substr($origTitle[0], 1, -1));
        $groupArray = implode(",", $group);
        //Check if the manga has a volume no if not then volume is not stored
        if(strpos($origTitle[2],' v.')){
            $origVol = preg_split("/ v\.| c\./", $origTitle[2]);
            $title = trim($origVol[0]);
            $volume = $origVol[1];
            $chapter = $origVol[2];
            /*if(strpos($chapter,'-')){
                $chapter = preg_split("/\b\d{1,}[-]?\d{1,}\b/", $chapter, 0, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
                print_r($chapter);
            }*/
        } else {
            $origVol = preg_split("/ c\./", $origTitle[2]);
            $title = trim($origVol[0]);
            $volume = "";
            $chapter = $origVol[1];
        }

        $origDesc = explode("<br>", $item->get_description());
        $description = $origDesc[0];
        $link = $item->get_link();

        $title = $conn->real_escape_string($title);

        /*echo "<b>Group: </b>".$groupArray."</br />";
        echo "<b>Name: </b>".$title."</br />";
        echo "<b>Volume: </b>".$volume."</br />";
//SOMETIMES MULTIPLE CHAPTERS SHOW UP E.G. 12-13. NEED TO DEAL WITH THIS SOMEHOW
        echo "<b>Chapter: </b>".$chapter."</br />";
        echo "<b>Description: </b>".$description."</br />";*/
        echo "<b>Link: </b>".$link."</br />";

        $groupId = array();
        foreach($group as $g){
            $g = $conn->real_escape_string(trim($g));
            $selectGroupId = "SELECT `id`, `name` FROM `group` WHERE `name` LIKE '$g' LIMIT 1";
            $selectGroupIdResult = $conn->query($selectGroupId);
            if($selectGroupIdResult->num_rows > 0){
                $dbGroup = $selectGroupIdResult->fetch_assoc();
                array_push($groupId, $dbGroup['id']);
            } else {
                $insertGroup = "INSERT INTO `group`(`name`) VALUES ('$g')";
                $insert = $conn->query($insertGroup);
                $returnGroup = "SELECT `id` FROM `group` WHERE `name` = '$g' ORDER BY `id` DESC";
                $return = $conn->query($returnGroup);
                if($return->num_rows > 0){
                    $dbGroup = $return->fetch_assoc();
                    array_push($groupId, $dbGroup['id']);
                }
            }
        }

        $selectMangaId = "SELECT `id`, `title` FROM `manga` WHERE `title` LIKE '$title' LIMIT 1";
        $selectMangaIdResult = $conn->query($selectMangaId);
        if($selectMangaIdResult->num_rows > 0){
            $dbManga = $selectMangaIdResult->fetch_assoc();
            $mangaId = $dbManga['id'];
        } else {
            $mangaId = scrap($link,$title);
        }

        $checkExistSql = "SELECT `id` FROM `release` WHERE `manga_id`='$mangaId' AND `volume`='$volume' AND `chapter`='$chapter' AND `date`='$description'";
        $checkExist = $conn->query($checkExistSql);

        if($checkExist->num_rows == 0){
            $insertReleaseSql = "INSERT INTO `release` (`manga_id`, `volume`, `chapter`, `date`) VALUES ('$mangaId','$volume','$chapter','$description')";
            $insertRelease = $conn->query($insertReleaseSql);

            $returnReleaseSql = "SELECT `id` FROM `release` WHERE `manga_id` = '$mangaId' ORDER BY `id` DESC LIMIT 1";
            $returnRelease = $conn->query($returnReleaseSql);
            if($returnRelease->num_rows > 0){
                        $dbRelease = $returnRelease->fetch_assoc();
                        $releaseId = $dbRelease['id'];
            }

            unset($mangaId);

            foreach($groupId as $gid){
                $insertGroupReleaseSql = "INSERT INTO `group_release`(`group_id`,`release_id`) VALUES ('$gid','$releaseId')";
                $insertGroupRelease = $conn->query($insertGroupReleaseSql);
            }
        } else {
            echo "Already exists.<br />";
        }

    }
}

$conn->close();
?>