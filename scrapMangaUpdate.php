<?php
include_once ('simple_html_dom.php');

function scrap($url,$title){
    global $conn;
    $feedTitle = $title;
    $html = new simple_html_dom();
    if (!empty($url)) {
        //$html->load_file($url);
        $html = file_get_html($url);
        //Check if the html page was actually loaded
        if (!empty($html)) {
            $title = $html->find("span[class=releasestitle tabletitle]", 0);
            //If the page has a title of the manga then proceed
            if($title!=NULL){
                $title = trim($title->plaintext);
                //echo $title;

                $description = $html->find('//*[@id="main_content"]/table[2]/tbody/tr/td/div[1]/div[3]/div/div[2]', 0);
                $description = $description->plaintext;
                $description = $conn->real_escape_string($description);

                //Check if the status contains the words Ongoing or Complete and store their counterparts otherwise don't store anything
                $status = $html->find('//*[@id="main_content"]/table[2]/tbody/tr/td/div[1]/div[3]/div/div[14]', 0);
                $status = $status->plaintext;
                if (strpos($status, '(Ongoing)') == true){
                    $status = "Ongoing";
                } elseif(strpos($status, '(Complete)') == true){
                    $status = "Completed";
                } else {
                    $status = "";
                }
                //echo $status;

                //Take the genre string returned and store each genre as an array
                $genre = $html->find('//*[@id="main_content"]/table[2]/tbody/tr/td/div[1]/div[4]/div/div[4]', 0);
                $genre = explode("&nbsp; ", $genre->plaintext);
                array_pop($genre);
                /*foreach ($genre as $g) {
                    echo $g.", ";
                }*/
                $genre = implode (", ", $genre);

                //Find all links with the title: Author Info and convert the names to lowercase in order to remove duplicates
                $author = $html->find('a[title="Author Info"] u');
                $author = array_map('strtolower', $author);
                $author = array_unique($author);
                /*foreach($author as $a){
                    echo strip_tags($a).", ";
                }*/
                $author = implode (", ", $author);
                $author = strip_tags($author);

                $release_date = $html->find('//*[@id="main_content"]/table[2]/tr/td/div[1]/div[4]/div[1]/div[16]/text()', 0);
                $release_date = trim($release_date->plaintext);
                if($release_date=='N/A'){
                    $release_date = '';
                }

                $url = explode('id=', $url);
                $url = $url[1];

                $cover = $html->find('//*[@id="main_content"]/table[2]/tbody/tr/td/div[1]/div[4]/div/div[2]/center/img', 0);

                if (!empty($cover)) {
                    $cover = file_get_contents($cover->src);
                    $img_path = '/images/cover/' . preg_replace('/[^\da-z]/i', "", strtolower($feedTitle)) . mt_rand().'.jpg';
                    $path = dirname(__FILE__) . $img_path;
                    file_put_contents($path, $cover);
                    $cover = ".".$img_path;
                }


                $insertSql = "INSERT INTO `manga`(`title`, `description`, `status`, `genre`, `author`, `release_date`, `cover`, `latest_chapter`, `url`, `added_date`) VALUES 
                ('" . $feedTitle . "','" . $description . "','" . $status . "','" . $genre . "','" . $author . "','" . $release_date . "','" . $cover . "','0','" . $url . "','" . date("Y-m-d") . "')";
                $insert = $conn->query($insertSql);
                $returnSql = "SELECT `id` FROM `manga` WHERE `url` = '$url' ORDER BY `id` DESC";
                $return = $conn->query($returnSql);
                if($return->num_rows > 0){
                    $dbManga = $return->fetch_assoc();
                    $mangaId = $dbManga['id'];
                    return $mangaId;
                }

            }
        }
    }

}

/*scrap('https://www.mangaupdates.com/series.html?id=122238');
scrap('https://www.mangaupdates.com/series.html?id=102872');
scrap('https://www.mangaupdates.com/series.html?id=100155');*/
?>